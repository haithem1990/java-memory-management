package com.java.memeory.management;

import java.util.HashMap;
import java.util.Map;

public class Memory {

    private Integer reference;
    private String name;
    private String type;
    private String value;
    private Map<String, Memory> links;

    public Memory(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public Memory(String name, String type, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public void addLink(String key, Memory link) {
        if (links == null) {
            links = new HashMap<>();
        }
        links.put(key, link);
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, Memory> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Memory> links) {
        this.links = links;
    }
}
