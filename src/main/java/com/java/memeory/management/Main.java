package com.java.memeory.management;

public class Main {
    public static void main(String[] args) {

        System.out.println("################ START NOTE #################");
        System.out.println("stack :");
        System.out.println(" * / every thread has its own stack ");
        System.out.println(" * / thread use the stack in order to save reference of created object");
        System.out.println("heap :");
        System.out.println(" * / object instance is created in the heap");
        System.out.println(" * / heap represent a memory provider for threads .");
        System.out.println(" * /  thread create an object in the heap and save the reference in it own stack .");
        System.out.println("################ END NOTE #################");
        Main main = new Main();
        main.start1();
        main.start2();
    }

    public void start1() {
        // first instruction
        Model model = new Model();
        // second instruction
        String c = "C";
        // third instruction
        model.setInitial(c);
        // fourth instruction
        System.out.println("start1()->" + model.getInitial());
    }

    public void start2() {
        StackProvider stackProvider = new StackProvider(Thread.currentThread().getName());
        LocalThreadProvider.threadLocal.set(stackProvider);
        // first instruction
        firstInstruction();
        // second instruction
        secondInstruction();
        // third instruction
        thirdInstruction();
        // fourth instruction
        fourthInstruction();
    }

    public void firstInstruction() {
        // the call of the Model constructor will add two memory location
        // the first is simple String with A as value and linked with the second memory
        // the second with type complex and has a link with the first
        // this is a simulation of how memory is managed
        // prepare memory for "A"
        Memory initialMemory = new Memory("String", "Simple", "A");
        // allocate memory in the heap for "A"
        initialMemory = HeapProvider.getInstance().allocateHeap(initialMemory);
        // prepare memory for Model instance
        Memory modelMemory = new Memory("Model", "Complex");
        // add initialReference as link to modelMemory
        modelMemory.addLink("initial", initialMemory);
        // add modelMemory to the stack
        StackProvider stackProvider = LocalThreadProvider.threadLocal.get();
        stackProvider.add("model", modelMemory);
    }

    public void secondInstruction() {
        StackProvider stackProvider = LocalThreadProvider.threadLocal.get();
        // prepare memory for "C"
        Memory cMemory = new Memory("String", "Simple", "C");
        stackProvider.add("c", cMemory);
    }

    public void thirdInstruction() {
        StackProvider stackProvider = LocalThreadProvider.threadLocal.get();
        Integer modelReference = stackProvider.getStack().get("model");
        Memory modelMemory = HeapProvider.getInstance().getMemoryByReference(modelReference);
        modelMemory.getLinks().remove("initial");
        Integer cReference = stackProvider.getStack().get("c");
        Memory cMemory = HeapProvider.getInstance().getMemoryByReference(cReference);
        modelMemory.getLinks().put("initial", cMemory);
    }

    public void fourthInstruction() {
        StackProvider stackProvider = LocalThreadProvider.threadLocal.get();
        Integer modelReference = stackProvider.getStack().get("model");
        Memory modelMemory = HeapProvider.getInstance().getMemoryByReference(modelReference);
        String initialContent = modelMemory.getLinks().get("initial").getValue();
        System.out.println("start2()->" + initialContent);
    }
}
