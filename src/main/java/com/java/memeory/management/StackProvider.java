package com.java.memeory.management;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StackProvider {

    private Map<String, Integer> stack;
    private String threadName;

    public StackProvider(String threadName) {
        stack = new ConcurrentHashMap<>();
        this.threadName = threadName;
        System.out.println("start stack for thread = " + threadName);
    }

    public Integer add(String name, Memory memory) {
        memory = HeapProvider.getInstance().allocateHeap(memory);
        stack.put(name, memory.getReference());
        return memory.getReference();
    }

    public Map<String, Integer> getStack() {
        return stack;
    }

    public void setStack(Map<String, Integer> stack) {
        this.stack = stack;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }
}
