package com.java.memeory.management;

import java.util.Arrays;

public class Model {

    public Model() {
    }

    private String initial = "A";

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }
}
