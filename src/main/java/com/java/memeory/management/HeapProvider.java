package com.java.memeory.management;

import java.util.HashMap;
import java.util.Map;

public class HeapProvider {
    private static HeapProvider heapProvider;
    private Integer reference;
    private Map<Integer, Memory> heap;

    private HeapProvider() {
        reference = 10000;
        heap = new HashMap<>();
    }

    public synchronized static HeapProvider getInstance() {
        if (heapProvider == null) {
            heapProvider = new HeapProvider();
        }
        return heapProvider;
    }

    public synchronized Memory allocateHeap(Memory memory) {
        reference = reference + 1;
        heap.put(reference, memory);
        memory.setReference(reference);
        return memory;
    }

    public synchronized Memory getMemoryByReference(Integer reference) {
        return heap.get(reference);
    }
}
